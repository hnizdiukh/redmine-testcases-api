/projects

  * name - Project name
  * key - Project id
  * description

/{projectId}/bug-list

  * bugs : array<bugId>

/{projectId}/bug-list/{bugId}

  * _id - bugId
  * screenshots
  * environment
  * number (Bug #`number`)
  * description
  * preconditions
  * steps : array
  * actual
  * expected
  * pageLink
  * screenshot
  * projectId

/{projectId}/collections

  * name
  * id - collectionId
  * description
  * projectId

/{projectId}/collections/{collectionId}/testcases

  * _id
  * screenshots
  * environment
  * number (Test case #`number`)
  * preconditions
  * steps : array
  * expected
  * screenshot
  * collectionId
  * projectId