const express = require('express');
const db = require('../db');
const { bugSchema } = require('../schemas');

const bugsCollection = db.get('testcases');
const projectsCollection = db.get('projects');
const router = express.Router({ mergeParams: true });

const singleBug = async (req, res, next) => {
  try {
    const { projectId, bugId } = req.params;

    const bugs = await bugsCollection.findOne({ _id: bugId, projectId });
    res.status(200);
    res.json(bugs);
  } catch (error) {
    res.status(404);
    next(error);
  }
};

const bugList = async (req, res, next) => {
  try {
    const { projectId } = req.params;

    const bugs = await bugsCollection.find({ projectId });
    res.status(200);
    res.json(bugs);
  } catch (error) {
    res.status(404);
    next(error);
  }
};

const addBug = async (req, res, next) => {
  try {
    const bug = req.body;
    const { projectId } = req.params;

    const project = await projectsCollection.findOne({ key: projectId.toLowerCase() });
    if (!project) {
      throw new Error(`No such project with key "${projectId}"`);
    }

    if (!bug.number) {
      bug.number = await bugsCollection.count({ projectId }) + 1;
    }

    bug.projectId = projectId;

    const validated = await bugSchema.validateAsync(bug,
      { stripUnknown: true, abortEarly: false }
    );

    await bugsCollection.insert(validated);

    res.status(201);
    res.json(validated);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

router.get('/', bugList);
router.post('/', addBug);
router.get('/:bugId', singleBug);

module.exports = router;
