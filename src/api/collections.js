const express = require('express');
const slugify = require('slugify');
const db = require('../db');
const testcases = require('./testcases');
const { collectionSchema } = require('../schemas');

const collectionsDB = db.get('collections');
const projectsCollection = db.get('projects');
const router = express.Router();

const collectionList = (req, res, next) => {
  try {
    const collections = collectionsDB.find({ projectId: req.params.projectId });

    res.status(200);
    res.json(collections);
  } catch (error) {
    res.status(404);
    next(error);
  }
};

const addCollection = async (req, res, next) => {
  try {
    const collection = req.body;
    const { projectId } = req.params;
    if (!Object.keys(collection).length) {
      throw new Error('Request body can not be empty');
    }

    collection.projectId = projectId;

    const project = await projectsCollection.findOne({ key: projectId.toLowerCase() });

    if (!project) {
      throw new Error(`No such project with key "${projectId}"`);
    }

    if (!collection.key) {
      collection.key = collection.name;
    }

    const duplicates = await collectionsDB.count(
      { projectId: projectId.toLowerCase(), id: collection.key }
    );

    if (duplicates) {
      throw new Error('Collection with such key is already exist on this project');
    }

    collection.key = slugify(collection.key.toString()).toLowerCase();
    const validated = await collectionSchema.validateAsync(collection,
      { stripUnknown: true, abortEarly: false }
    );

    await collectionsDB.insert(validated);

    res.status(201);
    res.json(validated);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

router.get('/:projectId/collections', collectionList);
router.post('/:projectId/collections', addCollection);

router.use('/', testcases);

module.exports = router;
