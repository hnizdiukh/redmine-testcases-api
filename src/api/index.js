const express = require('express');

const projects = require('./projects');

const router = express.Router();

router.get('/', (req, res) => {
  res.json({
    message: 'API - 👋🌎🌍🌏'
  });
});

router.use('/', projects);

module.exports = router;
