const express = require('express');
const slugify = require('slugify');

const db = require('../db');
const collections = require('./collections');
const bugs = require('./bugs');

const router = express.Router();
const projectsCollection = db.get('projects');
const { projectSchema } = require('../schemas');

const projectList = async (_, res, next) => {
  try {
    const projects = await projectsCollection.find();

    res.status(200);
    res.json(projects);
  } catch (error) {
    res.status(404);
    next(error);
  }
};

const addProject = async (req, res, next) => {
  try {
    const project = await projectSchema.validateAsync(req.body,
      { stripUnknown: true, abortEarly: false }
    );

    if (!project.key) {
      project.key = project.name;
    }

    project.key = slugify(project.key.toString()).toLowerCase();

    const duplicates = await projectsCollection.count({ key: project.key });
    if (duplicates) {
      throw new Error('Project with such key is already exist');
    }

    await projectsCollection.insert(project);

    res.status(201);
    res.json(project);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

router.get('/projects', projectList);
router.post('/projects', addProject);

router.use('/:projectId/bugs', bugs);
router.use('/', collections);

module.exports = router;
