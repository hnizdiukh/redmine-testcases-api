const express = require('express');
const db = require('../db');
const { testcaseSchema } = require('../schemas');

const testcasesCollection = db.get('testcases');
const projectsCollection = db.get('projects');
const collectionsDB = db.get('collections');
const router = express.Router();

const testcaseList = async (req, res, next) => {
  try {
    const { projectId, collectionId } = req.params;

    const testcases = await testcasesCollection.find({ projectId, collectionId });
    res.status(200);
    res.json(testcases);
  } catch (error) {
    res.status(404);
    next(error);
  }
};

const addTestcase = async (req, res, next) => {
  try {
    const testcase = req.body;
    const { projectId, collectionId } = req.params;

    const project = await projectsCollection.findOne({ key: projectId.toLowerCase() });
    if (!project) {
      throw new Error(`No such project with key "${projectId}"`);
    }

    const collection = await collectionsDB.findOne({ key: collectionId.toLowerCase() });
    if (!collection) {
      throw new Error(`No such collection with key "${collectionId}"`);
    }

    if (!testcase.number) {
      testcase.number = await testcasesCollection.count({ projectId, collectionId }) + 1;
    }

    testcase.projectId = projectId;
    testcase.collectionId = collectionId;

    const validated = await testcaseSchema.validateAsync(testcase,
      { stripUnknown: true, abortEarly: false }
    );

    await testcasesCollection.insert(validated);

    res.status(201);
    res.json(validated);
  } catch (error) {
    res.status(422);
    next(error);
  }
};

router.get('/:projectId/:collectionId/testcases', testcaseList);
router.post('/:projectId/:collectionId/testcases', addTestcase);

module.exports = router;
