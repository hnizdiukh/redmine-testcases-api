const db = require('monk')(process.env.MONGODB_URL);

module.exports = db;
