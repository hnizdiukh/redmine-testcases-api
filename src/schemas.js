const Joi = require('joi');

const testcaseSchema = Joi.object({
  screenshots: Joi.string(),
  environment: Joi.string(),
  number: Joi.number().integer(),
  preconditions: Joi.string(),
  steps: Joi.array().items(Joi.string()),
  expected: Joi.string().required(),
  screenshot: Joi.string(),
  collectionId: Joi.string().required(),
  projectId: Joi.string().required(),
});

const bugSchema = Joi.object({
  screenshots: Joi.string(),
  environment: Joi.string(),
  number: Joi.number().integer(),
  preconditions: Joi.string(),
  steps: Joi.array().items(Joi.string()),
  actual: Joi.string().required(),
  expected: Joi.string(),
  screenshot: Joi.string(),
  pageLink: Joi.string(),
  projectId: Joi.string().required(),
});

const collectionSchema = Joi.object({
  key: Joi.string().required(),
  name: Joi.string().required().min(2),
  description: Joi.string(),
  projectId: Joi.string()
});

const projectSchema = Joi.object({
  key: Joi.string(),
  name: Joi.string().required().min(2),
  description: Joi.string()
});

module.exports = { testcaseSchema, bugSchema, collectionSchema, projectSchema };
